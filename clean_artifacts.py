#!/usr/bin/env python3

import dateutil.parser
import time
from datetime import datetime
from dateutil.tz import tzutc

import requests

project_id = 'XXXXXXX'
token = 'XXXXXXXXX'
server = 'gitlab.com'
start_page = 1

dry_run = True

now = datetime.now(tz=tzutc())

print("Creating list of all jobs that currently have artifacts...")
url = f"https://{server}/api/v4/projects/{project_id}/jobs?per_page=100&page={start_page}"
with requests.Session() as session:
    while url:
        print(f"Processing page: {url}")
        response = session.get(
            url,
            headers={
                'private-token': token,
            },
        )

        if response.status_code in [500, 429]:
            print(f"Status {response.status_code}, retrying.")
            time.sleep(10)
            continue

        response.raise_for_status()
        response_json = response.json()
        for job in response_json:
            if not (created_at_str := job.get('created_at', None)):
                continue
            created_at = dateutil.parser.parse(created_at_str)
            job_id = job['id']
            if (now - created_at).days > 60:
                if job.get('artifacts_file', None) or job.get('artifacts', None):
                    if dry_run:
                        print(f" - dryrun: delete job_id {job_id} with created_at {created_at_str}")
                    else:
                        print(f"Processing job ID: {job_id}", end="")
                        delete_response = session.delete(
                            f"https://{server}/api/v4/projects/{project_id}/jobs/{job_id}/artifacts",
                            headers={
                                'private-token': token,
                            },
                        )
                        print(f" - status: {delete_response.status_code}")
            else:
                print(f" - skip job_id {job_id} with created_at {created_at_str}")

        url = response.links.get('next', {}).get('url', None)
