#!/bin/bash
#
# OpenVPN 2 start script using encrypted password file # with encfs in
# a secure way.
#
# Setup:
#
# 1. install encfs, openvpn and sudo:
#
# apt install sudo encfs openvpn
#
# 2. Setup an encrypted encfs container to store your ovpn profile and password:
#
# cd $HOME
# mkdir -p mnt/openvpn_cr
# encfs `pwd`/.openvpn_cr `pwd`/mnt/openvpn_cr
# 
# Create the encfs container with the same password as your sudo (user) password
# so it can be unlocked at once.
# 
# Then place your .opvn file as profile.ovpn in ~/mnt/openvpn_cr/profile.ovpn
# and add a file ~/mnt/openvpn_cr/profile.pass that contains your OpenVPN
# username and password on two separate lines.
# See https://openvpn.net/community-resources/reference-manual-for-openvpn-2-4/
# for more information about auth-user-pass.
#
# When your encfs container setup is done, you can unmount it with:
#
# fusermount -u ~/mnt/openvpn_cr
#
# 3. Make sure your user can sudo to root with your user password and
#    the SETENV permission. This works by default in Ubuntu.
#
# sudo -E bash
#
# 4. All done!
#


if [ "$1" = "step2" ]; then
	# this runs as root

	mkdir /run/start_openvpn
	chmod 700 /run/start_openvpn
	
	# reuse sudo password for decrypting container
	encfs -i 15 --extpass="$0" $HOME/.openvpn_cr /run/start_openvpn

	unset OVPN_START_PASSWORD

	openvpn --config /run/start_openvpn/profile.ovpn --auth-user-pass /run/start_openvpn/profile.pass

elif [ -n "$OVPN_START_PASSWORD" ]; then
	# act as an askpass program for encfs or sudo

	printf '%s\n' "$OVPN_START_PASSWORD"

else

	echo -n "Password: " 1>&2
	read -s OVPN_START_PASSWORD
	echo 1>&2

	export OVPN_START_PASSWORD
	export SUDO_ASKPASS="$0"
	
	# start a new process as root with inherited environment but isolated mount namespace
	sudo -A -E unshare -m "$0" step2

fi
